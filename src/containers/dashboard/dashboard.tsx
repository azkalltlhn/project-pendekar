// dashboard.jsx

import { Component } from "solid-js";
import { A } from "@solidjs/router";
import "@fortawesome/fontawesome-free/css/all.css";
import "./dashboard.css";

const Dashboard: Component = () => {
  return (
    <div class="container">
      <div class="sidebar">
        <div class="header">
          <div class="illustration">
            <img
              src="src/assets/img/logo-pendekar-dashboard.png"
              alt=""
              class="icon"
            />
          </div>
        </div>

        <div class="main">
          <div class="list-item">
            <A href="#">
              <img
                src="src/assets/img/icon-dashboard2.png"
                alt=""
                class="icon"
              />
              <span class="description-header">Dashboard</span>
            </A>
          </div>
        </div>
      </div>
      <div class="main-content">
        <h1>Hallo</h1>
      </div>
    </div>
  );
};

export default Dashboard;
