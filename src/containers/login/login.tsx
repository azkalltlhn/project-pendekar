import { createSignal, Component, onCleanup } from "solid-js";
import "./login.css";
import { A } from "@solidjs/router";
import "@fortawesome/fontawesome-free/css/all.css";

const Login: Component = () => {
  const [username, setUsername] = createSignal("");
  const [password, setPassword] = createSignal("");
  const [showPassword, setShowPassword] = createSignal(false);

  const handleLogin = () => {
    // Tambahkan logika login di sini
    console.log("Username:", username());
    console.log("Password:", password());
    console.log("showPassword:", showPassword());

    if (!username() || !password()) {
      alert("Harap isi semua kolom.");
    } else {
      // Lakukan logika login di sini
    }
  };

  onCleanup(() => {
    // Membersihkan nilai input saat komponen di-unmount
    setUsername("");
    setPassword("");
  });

  const ActionLogin = () => {
    console.log('hallo login button clicked');
    const dataUser = { username: "erp-last", email: "erp-last@gmail.com" };
    sessionStorage.setItem('userData', JSON.stringify(dataUser));
    window.location.assign('/');
  }

  const fetchLogin = async () => {
    try {
      const response = await fetch(`/api/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: username(),
          password: password(),
        }),
      });

      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        throw new Error('Failed to login');
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
  };

  const ActionLogin1 = async () => {
    try {
      const data = await fetchLogin();
      // Cek apakah data login berhasil atau tidak
      if (data) {
        // Login berhasil, Anda dapat menyimpan data pengguna di sessionStorage atau localStorage
        sessionStorage.setItem('userData', JSON.stringify(data));
        // Redirect ke halaman utama atau halaman lain yang sesuai
        window.location.assign('/');
      } else {
        // Handle login gagal di sini
        console.error('Login failed');
      }
    } catch (error) {
      // Handle kesalahan saat melakukan permintaan login
      console.error(error);
    }
  };

  return (
    <div class="flex lg:flex-row flex-col">
      <div class="res-atas flex flex-col lg:gap-16 lg:w-1/2 lg:overflow-hidden w-full p-2">
        <div class="flex sm:flex-row flex-col w-full justify-between mx-auto my-4 ">
          <div class="flex flex-row gap-4 items-center justify-center mb-4">
            <img
              class="business-education"
              src="src/assets/img/business-education-logo-2.png"
            />
            <div class="vertical-line"></div>
            <div class="flex flex-col gap-1 justify-center">
              <div class="text-wrapper-1">PENDEKAR</div>
              <div class="text-wrapper-2">
                Pojok Education
                <br />
                Career Scholarship
              </div>
            </div>
          </div>
          <div class="flex flex-row lg:gap-3 xl:gap-12 gap-12 items-start justify-center text-wrapper mr-14 ml-8">
            <p>Tentang Kami</p>
            <p>Program</p>
            <p>Testimoni</p>
          </div>
        </div>
        <div class="rectangle p-6 m-auto">
          <p class="mulai-aksimu-untuk md:text-lg text-xl">
            <span class="span md:text-lg text-xl">Mulai Aksimu</span>
            <span class="text-wrapper-8">&nbsp;</span>
            <span class="text-wrapper-9 md:text-lg text-xl">untuk</span>
            <span class="text-wrapper-8">&nbsp;</span>
            <span class="span md:text-lg text-xl">Dunia</span>
            <span class="text-wrapper-8 md:text-lg">
              {" "}
              <br />
            </span>
            <span class="span md:text-lg text-xl">yang lebih baik</span>
            <span class="text-wrapper-8">&nbsp;</span>
            <span class="text-wrapper-9 md:text-lg text-xl">untuk Semua!</span>
          </p>
          <img class="element" src="src/assets/img/2466249-1.png" />
        </div>
        <br />
        <br />
      </div>
      <div class="res-bawah flex flex-col lg:w-1/2 w-full gap-2 p-2">
        <div class="flex flex-row gap-6 items-center p-6 items-center">
          <img
            class="img"
            src="src/assets/img/business-education-logo-2-59B.png"
          />
          <div class="vertical-line-1"></div>
          <div class="flex flex-col justify-center">
            <div class="text-wrapper-10">PENDEKAR</div>
            <div class="text-wrapper-11 ">
              Pojok Education Career Scholarship
            </div>
          </div>
        </div>
        <div class="flex flex-col p-4 gap-8 mx-auto pr-8 w-4/5 items-center justify-center">
          <p class="text-wrapper-12 self-start">Masuk</p>
          <div class='password-toggle w-full'>
            <input
              type="text"
              class="overlap-5 p-2 w-full"
              name="username"
              placeholder="Username"
              value={username()}
              onInput={(e) => setUsername(e.target.value)}
            />
          </div>
          <div class="password-toggle w-full">
            <input
              type={showPassword() ? "text" : "password"}
              class="overlap-5 p-2 w-full"
              name="password"
              placeholder="Password"
              value={password()}
              onInput={(e) => setPassword(e.target.value)}
            />
            <i
              class={`fas ${showPassword() ? 'fa-eye' : 'fa-eye-slash'}`}
              onClick={() => setShowPassword(!showPassword())}
            ></i>
          </div>
          <button class="overlap-6 px-4 py-3 mt-5 w-full" onclick={() => ActionLogin1()}>
            <div class="text-wrapper-16 login-button">
            <a href="/dashboard">Masuk </a>
            </div>
          </button>
          <p class="belum-punya-akun">
            <span class="text-wrapper-14">Belum punya akun? </span>{" "}
            <span class="text-wrapper-15">
              <a href="/register">Daftar </a>
            </span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Login;